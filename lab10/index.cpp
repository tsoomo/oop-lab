#include <bits/stdc++.h>

using namespace std;


class Shape
{
public:
    int type;
    virtual double area() = 0;
    string getType(){
        if(type == 0 ){
            return "Circle";
        }else if(type == 1){
            return "Square";
        }else{
            return "Triangle";
        }
    }
};

class Circle : public Shape
{
private:
    double radius;

public:
    Circle(double r) {
        radius = r;
        type = 0;
    }
    double area() { return 3.14159 * radius * radius; }
};

class Square : public Shape
{
private:
    double side;

public:
    Square(double s){
        side = s;
        type = 1;
    }
    double area() { return side * side; }
};

class Triangle : public Shape
{
private:
    double base;

public:
    Triangle(double b){
        base = b;
        type = 2;
    }
    double area() { return 0.866 * base * base; }
};

template <typename T>
class LinkedList
{
private:
    struct Node
    {
        T data;
        Node *next;
    };
    Node *head;
    int size;

public:
    LinkedList() : head(nullptr), size(0) {}
    ~LinkedList()
    {
        while (head)
        {
            Node *temp = head;
            head = head->next;
            delete temp;
        }
    }
    void add(T t)
    {
        Node *newNode = new Node;
        newNode->data = t;
        newNode->next = nullptr;
        if (!head)
        {
            head = newNode;
        }
        else
        {
            Node *curr = head;
            while (curr->next)
            {
                curr = curr->next;
            }
            curr->next = newNode;
        }
        size++;
    }
    void insert(T t, int index)
    {
        if (index < 0 || index > size)
        {
            return;
        }
        Node *newNode = new Node;
        newNode->data = t;
        if (index == 0)
        {
            newNode->next = head;
            head = newNode;
        }
        else
        {
            Node *curr = head;
            for (int i = 0; i < index - 1; i++)
            {
                curr = curr->next;
            }
            newNode->next = curr->next;
            curr->next = newNode;
        }
        size++;
    }
    T get(int index)
    {
        if (index < 0 || index >= size)
        {
            throw "Index out of range";
        }
        Node *curr = head;
        for (int i = 0; i < index; i++)
        {
            curr = curr->next;
        }
        return curr->data;
    }
    void remove(int index)
    {
        if (index < 0 || index >= size)
        {
            return;
        }
        if (index == 0)
        {
            Node *temp = head;
            head = head->next;
            delete temp;
        }
        else
        {
            Node *curr = head;
            for (int i = 0; i < index - 1; i++)
            {
                curr = curr->next;
            }
            Node *temp = curr->next;
            curr->next = temp->next;
            delete temp;
        }
        size--;
    }
    int length()
    {
        return size;
    }
    void printSorted(){
        if(head == nullptr){
            cout<< "Linked list is empty" << endl;
            return;
        }

        T *array = new T[size];

        Node *curr = head;
        int index = 0;
        while(curr != nullptr){
            array[index] = curr->data;
            index++;
            curr = curr->next;
        }
        for(int i=0;i<size-1;i++){
            for(int j=0;j<size-i-1;j++){
                if(array[j]->area() > array[j+1]->area()){
                    swap(array[j], array[j+1]);
                }
            }
        }
        for(int i=0;i<size;i++){
            cout << "Shape #" << i + 1 << " - "<<array[i]->getType()<< " - Area: " << array[i]->area() << endl;
        }
        cout<<endl;
        delete[] array;
    }
};

int main()
{
    LinkedList<Shape *> shapes;

    // Generate a random number of shapes between 20 and 30
    int numShapes = rand() % 11 + 20;

    for (int i = 0; i < numShapes; i++)
    {
        // Generate a random shape type (0 = Circle, 1 = Square, 2 = Triangle)
        int shapeType = rand() % 3;

        // Generate a random dimension between 1 and 10
        double dimension = rand() % 10 + 1;

        Shape *shape;
        switch (shapeType)
        {
        case 0:
            shape = new Circle(dimension);
            break;
        case 1:
            shape = new Square(dimension);
            break;
        case 2:
            shape = new Triangle(dimension);
            break;
        default:
            cerr << "Invalid shape type" << endl;
            exit(1);
        }

        shapes.add(shape);
    }

    shapes.printSorted();
    
    for (int i = 0; i < shapes.length(); i++)
    {
        Shape *shape = shapes.get(i);
        delete shape;
    }

    return 0;
}
