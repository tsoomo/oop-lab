#include <iostream>
using namespace std;

//Daraalal hevleh function zarlah
void print_array(int a[], int n){
    for(int i=0;i<n;i++){
        cout<<a[i]<<" ";
    }
    cout<<endl;
}

//Usuhuur erembleh function zarlah
void bubble_sort_asc(int a[], int n){
    for (int i = 0; i < n - 1; i++){
        for (int j = 0; j < n - i - 1; j++){
            // Daraallsan 2 elementiin ihiig ni baruun tald ni gargah
            if (a[j] > a[j + 1]){
                int temp = a[j];
                a[j] = a[j+1];
                a[j+1] = temp;
            }
        }
    }
    //Daraallaa hevleh
    print_array(a,n);
}

void bubble_sort_desc(int a[], int n){
    for (int i = 0; i < n - 1; i++){
        for (int j = 0; j < n - i - 1; j++){
             // Daraallsan 2 elementiin bagiig ni baruun tald ni gargah
            if (a[j] < a[j + 1]){
                int temp = a[j];
                a[j] = a[j+1];
                a[j+1] = temp;
            }
        }
    }
    //Daraallaa hevleh
    print_array(a,n);
}

int main(){
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++){
        cin>>a[i];
    }
    //Usuhuur erembleh
    bubble_sort_asc(a,n);
    //Buurahaar erembleh
    bubble_sort_desc(a,n);
    return 0;
}