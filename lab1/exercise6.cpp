#include <iostream>
using namespace std;

int min(int a[], int n){
    // Хамгийн бага тоогоо хамгийн эхний элемент гэж үзье
    int min = a[0];
    for(int i=1;i<n;i++){
         // Бүх элементээр гүйж хэрэв min хувьсагчаас бага байвал уг элементийн утгыг min хувьсагчид онооно.
        if(a[i] < min){
            min = a[i];
        }
    }
}

int max(int a[], int n){
    // Хамгийн их тоогоо хамгийн эхний элемент гэж үзье
    int max = a[0];
    for(int i=1;i<n;i++){
        // Бүх элементээр гүйж хэрэв max хувьсагчаас их байвал уг элементийн утгыг max хувьсагчид онооно.
        if(a[i] > max){
            max = a[i];
        }
    }
    return max;
}

int main(){
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++){
        cin>>a[i];
    }
    cout<<"Hamgiin baga:"<<min(a,n)<<endl;
    cout<<"Hamgiin ih:"<<max(a,n)<<endl;
}