#include <iostream>
using namespace std;
// 3 тооны хамгийн ихийг буцаах функцаа тодорхойлно
int min(int x, int y, int z){
    // x нь хамгийн их үед x-ыг буцаана
    if(x >= y && x >= z){
        return x;
    }
    // y нь хамгийн их үyд x-ыг буцаана
    if(y >= x && y >= z){
        return x;
    }
    // x,y аль нь ч хамгийн их биш учир z-ыг буцаана
    return z;
}

int main() {
    int a,b,c;
    // 3 тоогоо гараас авна
    cin>>a>>b>>c;
    // Функцаас ирсэн утгыг дэлгэцэнд хэвлэнэ
    cout<<min(a,b,c);
    return 0;
}