#include <iostream>
using namespace std;

//Нийлбэр олох функцаа тодорхойлно
int sum(int a[], int n){
    int sum = 0;
    for(int i=0;i<n;i++){
        sum += a[i];
    }
    return sum;
}
//Үржвэр олох функцаа тодорхойлно
int product(int a[], int n){
    int prod = 1;
    for(int i=0;i<n;i++){
        prod *= a[i];
    }
    return prod;
}

int main(){
    int n;
    cin>>n;
    int a[n];
    // n тоогоо гараас авна
    for(int i=0;i<n;i++){
        cin>>a[i];
    }
    cout<< sum(a,n)<<endl;
    cout<< product(a,n);
}