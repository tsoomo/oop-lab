#include <bits/stdc++.h>
using namespace std;

class Shape {
    public:
        string name;
        void print_area();
};

class Shape_2d: public Shape {
    public:
        float x;
        float y;
        float l;
        void print_area();
        void print_length();
};

class Circle: public Shape_2d {
    public:
        void print_area(){
            cout << name << " toirogiin talbai: " << l * l * 3.14 <<endl;
        }
        void print_length(){
            cout << name << " toirgiin perimetr " << 2 * 3.14 * l << endl;
        }
        Circle(){
            x=0;
            y=0;
            l=2.0;
            name = "test";
        }
        Circle(float x, float y, float l, string name){
            x = x;
            y = y;
            l = l;
            name = name;
        }
};

class Square: public Shape_2d {
    public: 
        void print_area(){
            cout<<name << " kvadratiin talbai: " << l * l << endl;
        }
        void print_length(){
            cout<<name << " kvadratiin perimetr: " << l * 4 << endl;
        }
        Square(){
            x = 0;
            y = 0;
            l = 2;
            name = "square";
        }
};

class Triangle: public Shape_2d {
    public: 
        void print_area(){
            cout<<name << " gurvaljingiin talbai: " << l * l * 0.866 <<endl;
        }
        void print_length() {
            cout<<name << " gurvaljingiin perimeter: "<< 3 * l << endl;
        }
        Triangle(){
            x = 0;
            y = 0;
            l = 3;
            name = "test";
        }
};


int main()
{
    Circle c1;
    c1.x = 2;
    c1.y = 2;
    c1.l = 4;
    c1.name = "1-r";
    c1.print_area();
    c1.print_length();
    
    Square s1;
    s1.x = 0;
    s1.y = 4;
    s1.l = 2;
    s1.name = "1-r";
    s1.print_area();
    s1.print_length();
    
    Triangle t1;
    t1.print_area();
    t1.print_length();
    
    return 0;
}