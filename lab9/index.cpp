#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct Date {
    int year, day, month;
};
typedef struct Date date;

class Person {
    protected: 
        string name;
        string ssnum;
        int age;
    public:
        string getName() {
            return this->name;
        }
        string getSsnum() {
            return this->ssnum;
        }
        int getAge(){
            return this->age;
        }
        void setName(const string &name){
            this->name = name;
        }
        void setSsnum(const string &ssnum){
            this->ssnum = ssnum;
        }
        void setAge(int age){
            this->age = age;
        }
};


class Spouse: public Person {
    private:
        date anniversityDate;
    public:
        Spouse(){
            this->name = "default";
            this->ssnum = "default";
            this->age = 0;
            this->anniversityDate.year = 1970;
            this->anniversityDate.month = 1;
            this->anniversityDate.day =1;
        }

        Spouse(const string &name, const string $ssnum, int age, int year, int month, int day){
            this->name = name;
            this->ssnum = ssnum;
            this->age = age;
            this->anniversityDate.year = year;
            this->anniversityDate.month = month;
            this->anniversityDate.day = day;
        }

        date getAnniversityDate(){
            return this->anniversityDate;
        }

        void setAnniversityDate(int year, int month, int day){
            this->anniversityDate.year = year;
            this->anniversityDate.month = month;
            this->anniversityDate.day = day;
        }
};

class Child: public Person {
    private:
        string favoriteToy;
    public:
        Child(){
            this->name = "default";
            this->ssnum = "default";
            this->age = 0;
            this->favoriteToy = "default";
        }

        Child(const string &name, const string &ssnum, int age, const string &toy ){
            this->name = name;
            this->ssnum = ssnum;
            this->age = age;
            this->favoriteToy = toy;
        }

        string getFavoriteToy() {
            return this->favoriteToy;
        }

        void setFavoriteToy(string &toy){
            this->favoriteToy = toy;
        }
};

class Division {
    private :
        string DivisionName;
    public: 
        Division(){
            this->DivisionName = "default";
        }
        Division(const string &divisionName){
            this->DivisionName = divisionName;
        }
        string getDivisionName(){
            return this->DivisionName;
        }
        void setDivisionName(const string &divisionName){
            this->DivisionName = divisionName;
        }
};

class JobDescription {
    private:
        string description;
    public:
        JobDescription(){
            this->description = "default";
        }
        JobDescription(const string &description){
            this->description = description;
        }
        string getDescription(){
            return this->description;
        }
        void setDescription(const string &description){
            this->description = description;
        }
};

class Employee: public Person {
    private:
        string companyId;
        string title;
        date startDate;

        vector<JobDescription*>jd;
        Division *div;
        vector<Child*>children;
        Spouse *spouse;
    public:
        Employee(){
            this->name = "default";
            this->ssnum = "default";
            this->age = 0;
            this->companyId = "default";
            this->title = "default";
            this->startDate.year = 1970;
            this->startDate.month = 1;
            this->startDate.day = 1;
            this->spouse = NULL;
            this->div = NULL;
        }
        Employee(const string name, const string ssnum, int age, const string id, const string title, int year, int month, int day, Division *d, JobDescription *jd){
            this->name = name;
            this->ssnum = ssnum;
            this->age = age;
            this->companyId = id;
            this->title = title;
            this->startDate.year = year;
            this->startDate.month = month;
            this->startDate.day = day;
            this->div = d;
            this->jd.push_back(jd);
            this->spouse = NULL;
        }

        string getCompanyId(){
            return this->companyId;
        }
        string getTitle(){
            return this->title;
        }
        date getStartDate(){
            return this->startDate;
        }
        void setCompanyId(const string &id){
            this->companyId = id;
        }
        void setTitle(const string &title){
            this->title = title;
        }
        void setStartDate(int year, int month, int day){
            this->startDate.year = year;
            this->startDate.month = month;
            this->startDate.day = day;
        }
        void setDivision(Division *div){
            this->div = div;
        }
        void addJobDescription(JobDescription *jd){
            this->jd.push_back(jd);
        }
        void addChild(Child *child){
            this->children.push_back(child);
        }
        void setSpouse(Spouse *spouse){
            this->spouse = spouse;
        }
        void showData(){
            cout<< "Ажилтны мэдээлэл:"<<endl;
            cout<< this->name << " " << this->ssnum << " " << this->age << " " << this->companyId << " " << this->title << " " << this->startDate.year << "-" << this->startDate.month << "-" << this->startDate.day << endl;
            cout<< "Дивизион мэдээлэл:" << endl;
            cout<< this->div->getDivisionName() << endl;
            cout<< "Ажлын мэдээлэл:" << endl;
            for(int i=0;i<this->jd.size();i++){
                cout<<i+1<<"-р ажлын мэдээлэл: "<< this->jd[i]->getDescription()<<endl;
            } 
            if(this->spouse != NULL){
                cout<< "Гэр бүлийн мэдээлэл:"<<endl;
                cout<<this->spouse->getName()<<" "<<this->spouse->getSsnum()<<" "<<this->spouse->getAge()<< " " << this->spouse->getAnniversityDate().year << "-"<<this->spouse->getAnniversityDate().month<<"-"<<this->spouse->getAnniversityDate().day<<endl;
            }
            for(int i=0;i<this->children.size();i++){
                cout<< i+1 << "-р хүүхдийн мэдээлэл: "<<endl;
                cout<< children[i]->getName() << " " << children[i]->getSsnum() << " " << children[i]->getAge() << " " << children[i]->getFavoriteToy()<<endl;
            }
            cout<<endl<<endl;
        }
};

int main(){
    Division d1("d1");
    Division d2("d2");

    JobDescription j1("Worker");
    JobDescription j2("Car driver");
    JobDescription j3("Doctor");
    JobDescription j4("Teacher");

    Employee e1("Bat", "0001",18,"id1","title1",2018,12,1,&d2, &j1);
    Employee e2("Bold", "0002", 22, "id2", "title2", 2021,3,1, &d1, &j3);

    
    e1.showData();
    e2.showData();

    Spouse s1("Dulam", "0003", 18, 2019,12,1);
    Spouse s2("Misheel", "0004", 22, 2020,3,8);

    Child ch1("Erkhes","0005",2, "Pororo");
    Child ch2("Temuulen", "0006", 5, "Tayo");
    Child ch3("Enkhjin", "0007", 1, "Barbie");
    Child ch4("Namuun","0008", 3, "Car");

    e1.addChild(&ch1);
    e1.addChild(&ch2);
    e1.addChild(&ch3);
    e2.addChild(&ch4);

    e1.setSpouse(&s1);
    e2.setSpouse(&s2);

    e1.showData();
    e2.showData();

    return 0;
}