#include <iostream>
using namespace std;

void swap(int &num1, int &num2) // Хэрэглэгчийн функцаа зарлах
{
    int temp = num1; // temp хувьсагчид 1-р тооны утгыг заалтан хувьсагч ашиглан хадгалах
    num1 = num2;     // 2-р тооны заалтан хувьсагчаар дамжуулж 1-р тооны утгыг өөрчлөх
    num2 = temp;     // temp хувьсагчаа ашиглан 2-р тооны утгыг өөрчлөх
}

int main()
{
    int a, b;      // 2 тоогоо зарлах
    cin >> a >> b; // 2 тооны утгыг гараас авах
    cout << "Solihoos umnu a,b" << endl
         << a << " " << b << endl; // Солих функц дуудахаас өмнө хэвлэх
    swap(a, b);
    cout << "Solisnii daraa a,b" << endl
         << a << " " << b << endl; // Солих функц дуудсаны дараа хэвлэж үр дүнгээ харах
    return 0;
}