#include <bits/stdc++.h>
using namespace std;

class Employee
{
public:
    int no;
    string name;
    string position;
    float hour;

    void set_default()
    {
        no = 0;
        name = "";
        position = "ажилчин";
        hour = 0;
    }

    void set(int n, string na, string pos, float h)
    {
        no = n;
        name = na;
        position = pos;
        hour = h;
    }

    void get()
    {
        cout << "Дугаар: " << no << endl;
        cout << "Нэр: " << name << endl;
        cout << "Албан тушаал: " << position << endl;
        cout << "Ажилласан цаг: " << hour << endl;
    }

    float director_salary()
    {
        return hour * 6000;
    }

    float calculate_salary()
    {
        float salary = hour * 5500;
        if (position == "захирал")
        {
            salary += director_salary();
        }
        return salary;
    }

    bool add_hour(float h)
    {
        if (0 <= h && h <= 24)
        {
            hour += h;
            return true;
        }
        return false;
    }
};

int main()
{
    Employee e1, e2;
    e1.set(1, "Бат", "ажилчин", 0);
    e2.set(2, "Болд", "захирал", 10);
    e1.get();
    e2.get();
    e1.add_hour(10);
    e1.get();
    cout << e1.calculate_salary() << endl;
    cout << e2.calculate_salary() << endl;
    return 0;
}